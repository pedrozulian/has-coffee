const http = require('http');
const app = require('./app');
const port = process.env.PORT || 3000; // caso não preencher, usa a 3000 como padrão //

const server = http.createServer(app);

server.listen(port);
