import { Client } from "paho-mqtt";

function startConnect() {
    const clientId; // em breve o Id do usuário
    const host;
    const port;
    
    client = new Paho.MQTT.Client(host, Number(port), clientId);

    client.onConnectionLost = onConnectionLost;
    client.onMessageArrived = onMessageArrived;

    client.connect({ onSuccess: onConnect });
}
