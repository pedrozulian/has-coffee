CREATE TABLE IF NOT EXISTS usuarios (
    id_usuario INT NOT NULL AUTO_INCREMENT,
    token LONGTEXT NOT NULL,
    apelido VARCHAR(45) NOT NULL UNIQUE,
    termo VARCHAR(2) NOT NULL,
    PRIMARY KEY (id_usuario)
);

CREATE TABLE IF NOT EXISTS cafes (
      tipo VARCHAR(45) NOT NULL,
      tamanho INT NOT NULL,
      PRIMARY KEY (tipo)
);

CREATE TABLE IF NOT EXISTS cafe_feito (
    id_cafe INT NOT NULL AUTO_INCREMENT,
    id_usuario INT NOT NULL,
    tipo VARCHAR(45) NOT NULL,
    qtde_atual INT NOT NULL,
    created TIMESTAMP  NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id_cafe)
);

    ALTER TABLE cafe_feito
ADD FOREIGN KEY (id_usuario)
     REFERENCES usuarios(id_usuario);

    ALTER TABLE cafe_feito
ADD FOREIGN KEY (tipo)
     REFERENCES cafes(tipo);

   INSERT INTO cafes
                (tipo, tamanho)
        VALUES (1, 10),
               (2, 15),
               (3, 20);