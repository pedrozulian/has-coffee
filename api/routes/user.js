const express = require('express');
const mysql = require('../database/mysql').pool;
const bcrypt = require('bcrypt');
const { route } = require('../app');
const router = express.Router();

router.get('/get-users', (req, res, next) => {
    mysql.getConnection((error, conn) => {
        const query = `SELECT apelido, termo
                         FROM usuarios`;
        conn.query(query, (error, result, field) => {
            conn.release();
            if (error) { return res.status(500).send({ error : error }) }
            const response = {
                quantidade: result.length,
                usuario: result.map(user => {
                    return {
                        id_usuario: user.id_usuario,
                        apelido: user.apelido,
                        termo: user.termo
                    }
                })
            }
            res.status(200).send(response); // nesse response pega a qtde de usuários e seus respectivos nomes e termo
        });
    });
});

router.post('/user', (req, res, next) => {
    mysql.getConnection((err, conn) => {
        const query = `SELECT *
                         FROM usuarios
                        WHERE token =?`;
        conn.query(query, [req.body.token], (error, result, fields) => {
            conn.release();
            if (error) { return res.status(500).send({ error : error }) }
            const response = result.map(item => {
                return item.id_usuario
            });
            console.log(response)
            return res.status(200).send(response) // nesse result vem todos os dados de usuário
        });
    });
});

router.post('/register', (req, res, next) => {
    mysql.getConnection((err, conn) => {
        if (err) { return res.status(500).send({ error : err }) }
        const query = `SELECT *
                         FROM usuarios
                        WHERE apelido =?`;  // seleciona o apelido igual ao do novo registro
        conn.query(query, [req.body.apelido], (error, results) => {
            conn.release();
            if (error) { return res.status(500).send({ error : error }) }
            if (results.length > 0) {
                return res.status(422).send({ error : error })
            } else {
                const token = req.body.apelido + req.body.termo;
                bcrypt.hash(token, 0, (errBcrypt, hash) => {
                    if (errBcrypt) { return res.status(500).send({ error : 'bcrpyt error' }) }
                    const query2 = `INSERT INTO usuarios
                                                (token, apelido, termo)
                                         VALUES (?, ?, ?)`;
                    conn.query(query2, [hash, req.body.apelido, req.body.termo], (error, results) => {
                        if (error) { return res.status(500).send({ error : 'error query2'}) }
                        const response = {
                            token: hash,
                            usuarioCriado: {
                                apelido: req.body.apelido, 
                                termo: req.body.termo
                            }
                        }
                        return res.status(200).send(response);
                    });
                });
            }
        });
    });
});


router.post('/login', (req, res, next) => {
    mysql.getConnection((err, conn) => {
        if (err) { return res.status(500).send({ error : error }) }
        const query = `SELECT *
                         FROM usuarios
                        WHERE token =?`;    // pega todos os dados do usuário que contém o token
        conn.query(query, [req.body.token], (error, results, fields) => {
            conn.release();
            if (error) { return res.status(500).send({ error : error }) }
            const localToken = req.body.token;  // token que vem do localStorage
            const userToken = results.filter(user => {  
                return user.token == localToken // retorna se for o mesmo token
            })
            if (!userToken.length) {    // se não tiver token
                return res.status(203).send({ error : error }) // erro null
            } else {
                response = {
                    status: true,
                    usuario: results
                }
                return res.status(200).send(response)
            }
        });
    });
});

router.post('/new-coffee', (req, res, next) => {
    mysql.getConnection((err, conn) => {
        if (err) { return res.status(500).send({ error : err }) }
        const query = `SELECT id_usuario
                         FROM usuarios
                        WHERE token=?`;
        console.log(req.body.token);
        conn.query(query, [req.body.token], (error, result, fields) => {
            conn.release();
            if (error) { return res.status(500).send({ error : error }) }
            const normalObj = Object.assign({},result[0])
            console.log('normalObj', normalObj)
            const idUser = normalObj.id_usuario;
            console.log('idUser', idUser)
            const query2 = `INSERT INTO cafe_feito
                                        (id_usuario, tipo, qtde_atual)
                                 VALUES (?, ?, ?)`;
            conn.query(query2, [idUser, 1, 10], (error, results, fields) => {
                if (error) { return res.status(500).send({ error : error }) }
                return res.status(200).send({ response : results })
            })
        })
    })
})

router.get('/get-coffees', (req, res, next) => {
    mysql.getConnection((err, conn) => {
        if (err) { return res.status(500).send({ error : err }) }
        const query = `SELECT usuarios.id_usuario, usuarios.apelido,
                              cafe_feito.id_cafe, cafe_feito.qtde_atual
                         FROM usuarios
                   INNER JOIN cafe_feito
                           ON usuarios.id_usuario = cafe_feito.id_usuario
                          AND cafe_feito.qtde_atual > 0
                     ORDER BY id_cafe DESC
                        LIMIT 1;`;
        conn.query(query, (error, results, fields) => {
            conn.release();
            if (error) { return res.status(500).send({ error : error }) }
            return res.status(200).send(results)
        });
    });
});

router.put('/get-cup', (req, res, next) => {
    mysql.getConnection((err, conn) => {
        if (err) { return res.status(500).send({ error : err }) }
        const query = `UPDATE cafe_feito
                          SET qtde_atual = qtde_atual-1
                        WHERE id_cafe    =?`;
        conn.query(query, [req.body.id_cafe], (error, result, fields) => {
            conn.release();
            if (error) { return res.status(500).send({ error : error }) }
            return res.status(200).send({ response : result })
        });
    });
});

module.exports = router;