import { Component, OnInit } from '@angular/core';
import { UserService } from './../../services/user.service';
import { LocalstorageService } from './../../services/localstorage.service';
import { createNgModule } from '@angular/compiler/src/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(
    private userService: UserService,
    private localStorageService: LocalstorageService
  ) { }

  tokenUser;
  datasOfUser = [];

  ngOnInit() {
    this.tokenUser = this.loadingDatasUser();
  }

  loadingDatasUser() {
    const token = this.localStorageService.getValueToken().toString();
    return token;
  }

  onSubmit(){
    const data = { "token" : this.tokenUser };
    console.log(data);
    this.userService.newCoffee(data).subscribe(data => {
      console.log(data);
    });
  }

}
