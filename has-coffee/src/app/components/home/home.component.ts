import { LocalstorageService } from './../../services/localstorage.service';
import { Component, OnInit } from '@angular/core';
// import { AppRoutingService } from './app.routing.service';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  formulario: FormGroup;
  termos = ['Selecionar termo', 1, 2, 3, 4, 5, 6];
  usuarios = {
    apelido: null,
    termo: null
  };
  tokenUser: any;
  valueOfTokenUser;

  constructor(
    private userService: UserService,
    private localStorage: LocalstorageService
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.formulario = new FormGroup({
      apelido: new FormControl(this.usuarios.apelido),
      termo: new FormControl(this.termos)
    });
  }

  onSubmit() {
    this.userService.registerUser(this.formulario.value)
    .subscribe(data => {
      console.log(data);
      this.manageTokenLocalStorage(data);
    });
    console.log('value', this.formulario.value);
  }

  manageTokenLocalStorage(data) {
    this.tokenUser = data.token;
    if (!this.tokenUser) {
      console.log('Não existe Token');
    } else {
      this.localStorage.insertTokenLocalStorage(this.tokenUser);
    }
  }

  redirectToConnection() {
    this.valueOfTokenUser = this.localStorage.getValueToken();
    if (!this.valueOfTokenUser) {
      return false;
    } else {
      return true;
    }
  }
}
