import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {

  allCoffees = [];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
    this.loadingDatas();
  }

  loadingDatas() {
    this.userService.getCoffees().subscribe(coffees => {
      this.allCoffees = [];
      coffees.map(coffee => this.allCoffees.push(coffee));
    });
  }

  getCup(id_cafe) {
    const data = { id_cafe };
    this.userService.getCup(data).subscribe(data => {
      console.log(data);
    });
    this.loadingDatas();
  }

}
