import { Component, OnInit } from '@angular/core';
// import { AppRoutingService } from './app.routing.service';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from './services/auth.service';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor(
    private userService: UserService,
    private authService: AuthService
    ) {}

  ngOnInit() {

  }

}
