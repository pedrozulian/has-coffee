import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  tokenLocalStorage;
  tokenValue;

  constructor(
    private router: Router,
    private userService: UserService
  ) { }

  getTokenLocalStorage() {
    this.tokenLocalStorage = window.localStorage.getItem('access-hc');
    if (!this.tokenLocalStorage) {
      return console.log('Não possui tokenLocalStorage');
    } else {
      return this.tokenLocalStorage = true;
    }
  }

  getValueToken() {
    this.tokenValue = window.localStorage.getItem('access-hc');
    return this.tokenValue;
  }

  insertTokenLocalStorage(data) {
    const token = data;
    window.localStorage.setItem('access-hc', token);
  }

}
