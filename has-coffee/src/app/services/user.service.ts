import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public API_URL = environment.API_URL;

  constructor(
    private http: HttpClient
  ) { }

  getUsers() {
    return this.http.get<any[]>(`${this.API_URL}/get-users`);
  }

  getUser(data) {
    return this.http.post(`${this.API_URL}/user`, data);
  }

  registerUser(data: { apelido: string, termo: string }) {
    return this.http.post<any[]>(`${this.API_URL}/register`, data);
  }

  login(data: {token: string}) {
    return this.http.post(`${this.API_URL}/login`, data);
  }

  getCoffees(): Observable<any> {
    return this.http.get(`${this.API_URL}/get-coffees`);
  }

  getCup(data): Observable<any> {
    return this.http.put(`${this.API_URL}/get-cup`, data);
  }

  newCoffee(data) {
    return this.http.post<any[]>(`${this.API_URL}/new-coffee`, data);
  }
}
